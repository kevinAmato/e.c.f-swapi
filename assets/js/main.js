"use strict";

var API_ROOT = "https://swapi.co/api/"
var selectElmt = document.getElementById("select")

function apiCall(resource, method, onsuccess ) {
  var request = new XMLHttpRequest()

  request.onreadystatechange = function (event) {
    if (this.readyState === XMLHttpRequest.DONE) {
      if (this.status === 200) {
        onsuccess(this.responseText)

      } else {
        console.log("API call failed")
      }
    }
  }

  request.open(method, API_ROOT + resource , true)
  request.send(null)
}

/*-----------------------------------------------------------------*/


function getPlanetList(onsuccess) {


  var valeurSelectionnee = selectElmt.options[selectElmt.selectedIndex].value
  var searchedPlanet = document.getElementById("planet-search").value
  console.log(searchedPlanet);

  if(searchedPlanet !== ""){
    apiCall(valeurSelectionnee+ "?search=" + searchedPlanet, "GET", function (response) {
       onsuccess(JSON.parse(response))
    })
  } else {
    apiCall(valeurSelectionnee, "GET", function (response) {
      onsuccess(JSON.parse(response))

    })
  }

}

/*---------------------------------------------------------------------*/

function handlePlanetSearch(e) {

  e.preventDefault()
  //var searchedPlanet = document.getElementById("planet-search").value
  var planetOutput = document.getElementById("planet-output")

  //affichage de la saisie
  //alert(searchedPlanet)

  while (planetOutput.firstChild) {
    planetOutput.removeChild(planetOutput.firstChild)
  }

  getPlanetList(function (response) {
    console.log(response);

    var ulElement = document.createElement("ul")
    for (var planet of response.results) {
      var liElement = document.createElement("li")
        if(selectElmt.value === "films"){
          liElement.textContent = planet.title
        }else {
          liElement.textContent = planet.name
        }
      ulElement.appendChild(liElement)
    }
    planetOutput.appendChild(ulElement)
  })
}


document.addEventListener("DOMContentLoaded", function () {
  document.getElementById("planet-search-form")
    .addEventListener("submit", handlePlanetSearch)
})

function getSaisie(){
  alert(searchedPlanet)
  console.log(searchedPlanet)
}
